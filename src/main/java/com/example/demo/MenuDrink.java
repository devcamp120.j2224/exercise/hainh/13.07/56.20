package com.example.demo;

public class MenuDrink {
    String maNuocUong;
    String tenNuocUong;
	String ngayTao  ;
	String ngayCapNhat;
	String ghiChu = null ;
	int donGia;
  

    // constructor
    public MenuDrink(String maNuocUong , String tenNuocUong , String ngayTao , String ngayCapNhat , String ghiChu , int donGia){
        this.maNuocUong = maNuocUong ;
        this.tenNuocUong = tenNuocUong;
        this.ngayTao = ngayTao ; 
        this.ngayCapNhat = ngayCapNhat ;
        this.donGia = donGia ;
    }

    public void setMaNuocUong(String maNuocUong){
        this.maNuocUong = maNuocUong ;
    }

    public String getMaNuocUong(){
        return maNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong){
        this.tenNuocUong = tenNuocUong ;
    }

    public String getTenNuocUong(){
        return tenNuocUong;
    }

    public void setNgayTao(String ngayTao){
        this.ngayTao = ngayTao ;
    }

    public String getNgayTao(){
        return ngayTao;
    }

    public void setNgayCapNhat(String ngayCapNhat){
        this.ngayCapNhat = ngayCapNhat ;
    }

    public String getNgayCapNhat(){
        return ngayCapNhat;
    }

    public void setDonGia(int donGia){
        this.donGia = donGia ;
    }

    public int getDonGia(){
        return donGia;
    }

    @Override
    public String toString(){
        return "{maNuocUong : " + this.maNuocUong + 
        ", tenNuocUong" + this.tenNuocUong + 
        ", ngayTao : " +  this.ngayTao + 
        ", ngayCapNhat : " + this.ngayCapNhat + 
        ", ghiChu : " + this.ghiChu + 
        ", donGia : " + this.donGia + "}";
    }
}
